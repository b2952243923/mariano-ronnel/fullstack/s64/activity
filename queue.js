let collection = [];

// Function to print the queue elements.
function print() {
    return collection;
}

// Function to enqueue a new element.
function enqueue(item) {
    collection[size()] = item;
    return collection;
}

// Function to dequeue the first element.
function dequeue() {
    if (collection.length === 0) {
        return collection;
    }

    for (let i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1];
    }
    collection.length--;

    return collection;
}



// Function to get the first element.
function front() {
    if (isEmpty()) {
        return undefined;
    }
    return collection[0];
}

// Function to get the size of the queue.
function size() {
    let count = 0;
    for (let i = 0; i < collection.length; i++) {
        count++;
    }
    return count;
}

// Function to check if the queue is empty.
function isEmpty() {
    return size() === 0;
}


// Write the queue functions below.

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};